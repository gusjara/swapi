# SWAPI

Proyecto creado en laravel 9 con PHP 8.0 y bases de datos MySQL.

Pasos
- clonar el repositorio
- run composer install
- si no se creo un archivo .env, crearlo con `cp .env.example .env`
- crear una base de datos en mysql
-  modificar en el .env las variables `DB_HOST por tu host` `DB_DATABASE nombre de la base que creaste` `DB_USERNAME Y DB_PASSWORD por los datos de tu base de datos`
- correr en la terminal `php artisan key:generate`
- correr en la terminal `php artisan jwt:secret` y se debe crear dos variables en el archivo  .env como estas `JWT_SECRET=mH08HguAFJZLxxxxxxx` y `JWT_ALGO=HS256`
- Agregar en el .env estas dos variables
	- `STARWARS_API_BASE_URL='https://swapi.py4e.com/api/'`
	- `SW_API_URL='https://swapi.py4e.com'`
- correr desde una terminal con `php artisan serve` en el navegador correría en  `127.0.0.1:8000`
- Cuenta de 8 rutas: 
	- /api/register -> registro de usuario para obtener el token
	- /api/login ->login de usuario
	- /api/people -> get all people and can pass ?page=xx with page number
	- /api/people/{id} -> get a people by id
	- /api/planets -> get all planet in each query show 10 planets and ?page=xx
	- /api/planets/{id} -> get a planet by id
	- /api/vehicles -> get a list of vehicles and ?page=xx to go to prev or next page
	- /api/vehicles/{id} -> get a vehicle by id

La api que brindaron dejo de funcionar, la cual es: `https://swapi.dev/` por tal caso pase a usar esta otra: `https://swapi.py4e.com`

La url de en un servidor es `https://swapi.404webs.com`

Dejo una url para una colección de postman si es requerida.
`https://www.getpostman.com/collections/9e53e8840896653618a8`