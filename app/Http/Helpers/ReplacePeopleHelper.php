<?php

namespace App\Http\Helpers;

class ReplacePeopleHelper
{
    /**
     * @param string $prev
     * @return string
     */
    public function replacePrevious(String $prev) :String
    {
        $previous = str_replace(env('SW_API_URL'), env('APP_URL'), $prev);
    
        return $previous;
    }

    /**
     * @param string $prev
     * @return string
     */
    public function replaceNext(String $next) :String
    {
        $_next = str_replace(env('SW_API_URL'), env('APP_URL'), $next);
    
        return $_next;
    }

    /**
     * @param string $url
     * @return string
     */
    public function replaceUrl(String $url) :String
    {
        $_url = str_replace(env('SW_API_URL'), env('APP_URL'), $url);

        return $_url;
    }

    /**
     * @param array $results
     * @return Array
     */
    public function filterResults(array $results) :Array
    {
        foreach($results as &$res){            
            
            unset($res['created']);
            unset($res['edited']);
            unset($res['films']);
            unset($res['species']);
            unset($res['starships']);
            
            $res['url'] = $this->replaceUrl($res['url']);
            $res['homeworld'] = $this->filterHomeworld($res['homeworld']);
            $res['vehicles'] = $this->filterVehicles($res['vehicles']);
            // $res['films'] = $this->filterFilms($res['films']);
            // $res['species'] = $this->filterSpecies($res['species']);
            // $res['starships'] = $this->filterStarships($res['starships']);
        }

        // dd($results);
        return $results;
    }

    /**
     * @param string $homeworld
     * @return string
     */
    public function filterHomeworld(String $homeworld) :String
    {
        $_homeworld = str_replace(env('SW_API_URL'), env('APP_URL'), $homeworld);

        return $_homeworld;
    }

    /**
     * @param array $films
     * @return Array
     */
    public function filterFilms(array $films) :Array
    {
        foreach($films as &$film){
            $film = str_replace(env('SW_API_URL'), env('APP_URL'), $film);
        }
        return $films;
    }

    /**
     * @param array $species
     * @return Array
     */
    public function filterSpecies(array $species) :Array
    {
        foreach($species as &$specie){
            $specie = str_replace(env('SW_API_URL'), env('APP_URL'), $specie);
        }
        return $species;
    }

    /**
     * @param array $vehicles
     * @return Array
     */
    public function filterVehicles(array $vehicles) :Array
    {
        foreach($vehicles as &$vehicle){
            $vehicle = str_replace(env('SW_API_URL'), env('APP_URL'), $vehicle);
        }

        return $vehicles;
    }

    /**
     * @param array $starships
     * @return Array
     */
    public function filterStarships(array $starships) :Array
    {
        foreach($starships as &$starship){
            $starship = str_replace(env('SW_API_URL'), env('APP_URL'), $starship);
        }
        
        return $starships;
    }

    /**
     * @param array $result
     * @return Array
     */
    public function filterResult(array $result) :Array
    {
        unset($result['created']);
        unset($result['edited']);
        unset($result['films']);
        unset($result['species']);
        unset($result['starships']);

        $result['url'] = $this->replaceUrl($result['url']);
        $result['homeworld'] = $this->filterHomeworld($result['homeworld']);
        $result['vehicles'] = $this->filterVehicles($result['vehicles']);
        // $result['films'] = $this->filterFilms($result['films']);
        // $result['species'] = $this->filterSpecies($result['species']);
        // $result['starships'] = $this->filterStarships($result['starships']);

        // dd($result);
        return $result;
    }

}