<?php

namespace App\Http\Helpers;

class ReplaceVehicleHelper
{
    /**
     * @param string $prev
     * @return string
     */
    public function replacePrevious(String $prev) :String
    {
        $previous = str_replace(env('SW_API_URL'), env('APP_URL'), $prev);
    
        return $previous;
    }

    /**
     * @param string $prev
     * @return string
     */
    public function replaceNext(String $next) :String
    {
        $_next = str_replace(env('SW_API_URL'), env('APP_URL'), $next);
    
        return $_next;
    }

    /**
     * @param string $url
     * @return string
     */
    public function replaceUrl(String $url) :String
    {
        $_url = str_replace(env('SW_API_URL'), env('APP_URL'), $url);

        return $_url;
    }

    /**
     * @param array $results
     * @return Array
     */
    public function filterResults(array $results) :Array
    {
        foreach($results as &$res){            
            
            unset($res['created']);
            unset($res['edited']);
            unset($res['films']);
            
            $res['url'] = $this->replaceUrl($res['url']);
            $res['pilots'] = $this->filterPilots($res['pilots']);
        }

        return $results;
    }


    /**
     * @param array $result
     * @return Array
     */
    public function filterResult(array $result) :Array
    {
        unset($result['created']);
        unset($result['edited']);
        unset($result['films']);

        $result['url'] = $this->replaceUrl($result['url']);
        $result['pilots'] = $this->filterPilots($result['pilots']);

        return $result;
    }

    /**
     * @param array $pilots
     * @return Array
     */
    public function filterPilots(array $pilots) :Array
    {
        foreach($pilots as &$pilot){
            $pilot = str_replace(env('SW_API_URL'), env('APP_URL'), $pilot);
        }
        return $pilots;
    }

}