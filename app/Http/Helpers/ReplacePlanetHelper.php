<?php

namespace App\Http\Helpers;

class ReplacePlanetHelper
{
    /**
     * @param string $prev
     * @return string
     */
    public function replacePrevious(String $prev) :String
    {
        $previous = str_replace(env('SW_API_URL'), env('APP_URL'), $prev);
    
        return $previous;
    }

    /**
     * @param string $prev
     * @return string
     */
    public function replaceNext(String $next) :String
    {
        $_next = str_replace(env('SW_API_URL'), env('APP_URL'), $next);
    
        return $_next;
    }

    /**
     * @param string $url
     * @return string
     */
    public function replaceUrl(String $url) :String
    {
        $_url = str_replace(env('SW_API_URL'), env('APP_URL'), $url);

        return $_url;
    }

    /**
     * @param array $results
     * @return Array
     */
    public function filterResults(array $results) :Array
    {
        foreach($results as &$res){            
            
            unset($res['created']);
            unset($res['edited']);
            unset($res['films']);
            
            $res['url'] = $this->replaceUrl($res['url']);
            $res['residents'] = $this->filterResidents($res['residents']);
            // $res['films'] = $this->filterFilms($res['films']);
        }

        return $results;
    }


    /**
     * @param array $films
     * @return Array
     */
    public function filterFilms(array $films) :Array
    {
        foreach($films as &$film){
            $film = str_replace(env('SW_API_URL'), env('APP_URL'), $film);
        }
        return $films;
    }

    /**
     * @param array $residents
     * @return Array
     */
    public function filterResidents(array $residents) :Array
    {
        foreach($residents as &$resident){
            $resident = str_replace(env('SW_API_URL'), env('APP_URL'), $resident);
        }
        return $residents;
    }



    /**
     * @param array $result
     * @return Array
     */
    public function filterResult(array $result) :Array
    {
        unset($result['created']);
        unset($result['edited']);
        unset($result['films']);

        $result['url'] = $this->replaceUrl($result['url']);
        $result['residents'] = $this->filterResidents($result['residents']);

        return $result;
    }

}