<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\StarwarsApiService;
use Illuminate\Support\Facades\Auth;

class StarwarsController extends Controller
{
    /**
     *
     * @var StarwarsApiService
     */
    public $sw_service;

    /**
     * 
     */
    public function __construct(StarwarsApiService $sw_service)
    {
        // $this->middleware('auth:sanctum');

        $this->middleware('auth:api');

        $this->sw_service = $sw_service;
    }
    
    /**
     * Display a listing of the resource People.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPeopleAll(Request $request)
    {
        $page = $request->page ?? null;
        $limit = $request->limit ?? null;
        $offset = $request->offset ?? null;
        
        $response = $this->sw_service->getPeopleAll($page, $limit, $offset);

        return response()->json($response, 201);
    }

    /**
     * Display the specified resource of people.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPeopleById($id)
    {
        $response = $this->sw_service->getPeopleById($id);

        return response()->json($response, 201);
    }

    /**
     * Display a listing of the resource Planets.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPlanetsAll(Request $request)
    {
        $page = $request->page ?? null;
        $limit = $request->limit ?? null;
        $offset = $request->offset ?? null;

        $response = $this->sw_service->getPlanetsAll($page, $limit, $offset);

        if($response) return response()->json($response, 201);

        return response()->json([
            'success' => false,
        ], 404);

    }

    /**
     * Display the specified resource of Planets.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPlanetsById($id)
    {   
        $response = $this->sw_service->getPlanetsById($id);

        if($response) return response()->json($response, 201);

        return response()->json([
            'success' => false,
        ], 404);
    }

    /**
     * Display a listing of the resource Vehicles.
     *
     * @return \Illuminate\Http\Response
     */
    public function getVehiclesAll(Request $request)
    {
        $page = $request->page ?? null;
        $limit = $request->limit ?? null;
        $offset = $request->offset ?? null;

        $response = $this->sw_service->getVehiclesAll($page, $limit, $offset);

        if($response) return response()->json($response, 201);

        return response()->json([
            'success' => false,
        ], 404);
    }

    /**
     * Display the specified resource of Vehicles.
     *
     * @return \Illuminate\Http\Response
     */
    public function getVehiclesById($id)
    {
        $response = $this->sw_service->getVehiclesById($id);

        if($response) return response()->json($response, 201);

        return response()->json([
            'success' => false,
        ], 404);
    }

}
