<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use GuzzleHttp\Client as GuzzleClient;
use App\Http\Helpers\ReplacePeopleHelper;
use App\Http\Helpers\ReplacePlanetHelper;
use GuzzleHttp\Exception\GuzzleException;
use App\Http\Helpers\ReplaceVehicleHelper;

class StarwarsApiService
{

    /**
     * @var
     */
    private $api_url;

    /**
     * @var GuzzleClient
     */
    private $client;

    /**
     * @var ReplacePeopleHelper
     */
    private $replacePeople;

    /**
     * @var ReplacePlanetHelper
     */
    private $replacePlanet;

    /**
     * @var ReplaceVehicleHelper
     */
    private $replaceVehicle;

    public function __construct(GuzzleClient $client, ReplacePeopleHelper $replacePeople, ReplacePlanetHelper $replacePlanet, ReplaceVehicleHelper $replaceVehicle)
    {
        $this->client = $client;
        $this->api_url = config('starwars-api.sw-api.base_url');
        $this->replacePeople = $replacePeople;
        $this->replacePlanet = $replacePlanet;
        $this->replaceVehicle = $replaceVehicle;
    }

    /**
     * endpoint https://swapi.dev/api/people/
     *
     * @return array
     */
    public function getPeopleAll($page = 1, $limit = null, $offset = null) :array
    {
        // endpoint
        $endpoint = 'people/';

        // url
        $url = $this->api_url . $endpoint . '?page=' . $page;

        try {

            // to work with cache
            $key = '_endpoint_' . $endpoint . '_page_' . $page . '_limit_' . $limit . '_offset_' . $offset;
            // get from cache
            $response = Cache::get($key);

            if(empty($response)){
                $request = $this->client->request('GET', $url);

                $response = json_decode($request->getBody()->getContents(), true);
                // $response = $request->getBody()->getContents();

                // replace results
                if(!is_null($response['previous']) || !empty($response['previous'])){
                    $response['previous'] = $this->replacePeople->replacePrevious($response['previous']);
                }
                // $response['previous'] = $this->replacePeople->replacePrevious($response['previous']);
                if(!is_null($response['next']) || !empty($response['next'])){
                    $response['next'] =  $this->replacePeople->replaceNext($response['next']);
                }
                
                if(count($response['results'])) {
                    $response['results'] = $this->replacePeople->filterResults($response['results']);
                }                
                
                // save on cache 24 hours
                Cache::put($key, serialize($response), 86400);

                return $response;

            }else{
                // return from cache 
                return unserialize($response);
            }
        } catch (GuzzleException $e) {
            return false;
        }catch (\Exception $e) {
            return false;
        }
    }

    /**
     * endpoint https://swapi.dev/api/people/1/
     *
     * @return array
     */
    public function getPeopleById($id) :array
    {
        // endpoint
        $endpoint = 'people/' . $id;

        // url
        $url = $this->api_url . $endpoint;

        try {

            // to work with cache
            $key = '_endpoint_' . $endpoint;
            // get from cache
            $response = Cache::get($key);

            if(empty($response)){
                $request = $this->client->request('GET', $url);

                $response = json_decode($request->getBody()->getContents(), true);
                // $response = $request->getBody()->getContents();
                
                $response = $this->replacePeople->filterResult($response);
                
                // save on cache 24 hours
                Cache::put($key, serialize($response), 86400);

                return $response;

            }else{
                // return from cache 
                return unserialize($response);
            }
        } catch (GuzzleException $e) {
            return false;
        }catch (\Exception $e) {
            return false;
        }

    }

    /**
    * endpoint https://swapi.dev/api/planets/
    *
    * @return array|mixed
    */
    public function getPlanetsAll($page = 1, $limit = null, $offset = null) :mixed
    {
        // endpoint
        $endpoint = 'planets/';

        // url
        $url = $this->api_url . $endpoint . '?page=' . $page;

        try {

            // to work with cache
            $key = '_endpoint_' . $endpoint . '_page_' . $page . '_limit_' . $limit . '_offset_' . $offset;
            // get from cache
            $response = Cache::get($key);

            if(empty($response)){
                $request = $this->client->request('GET', $url);

                $response = json_decode($request->getBody()->getContents(), true);
                // $response = $request->getBody()->getContents();

                // replace results
                if(!is_null($response['previous']) || !empty($response['previous'])){
                    $response['previous'] = $this->replacePlanet->replacePrevious($response['previous']);
                }

                if(!is_null($response['next']) || !empty($response['next'])){
                    $response['next'] =  $this->replacePlanet->replaceNext($response['next']);
                }

                if(count($response['results'])) {
                    $response['results'] = $this->replacePlanet->filterResults($response['results']);
                }                

                // save on cache 24 hours
                Cache::put($key, serialize($response), 86400);

                return $response;

            }else{
                // return from cache 
                return unserialize($response);
            }
        } catch (GuzzleException $e) {
            return false;
        }catch (\Exception $e) {
            return false;
        }
    }

    /**
    * endpoint https://swapi.dev/api/planets/{id}
    *
    * @return array|mixed
    */
    public function getPlanetsById($id)
    {
        // endpoint
        $endpoint = 'planets/' . $id;

        // url
        $url = $this->api_url . $endpoint;

        try {

            // to work with cache
            $key = '_endpoint_' . $endpoint;
            // get from cache
            $response = Cache::get($key);

            if(empty($response)){
                $request = $this->client->request('GET', $url);

                $response = json_decode($request->getBody()->getContents(), true);
                // $response = $request->getBody()->getContents();
                
                $response = $this->replacePlanet->filterResult($response);
                
                // save on cache 24 hours
                Cache::put($key, serialize($response), 86400);

                return $response;

            }else{
                // return from cache 
                return unserialize($response);
            }
        } catch (GuzzleException $e) {
            return false;
        }catch (\Exception $e) {
            return false;
        }
    }
    
    /**
    * endpoint https://swapi.dev/api/vehicles/
    *
    * @return array|mixed
    */
    public function getVehiclesAll($page = 1, $limit = null, $offset = null) :mixed
    {
        // endpoint
        $endpoint = 'vehicles/';

        // url
        $url = $this->api_url . $endpoint . '?page=' . $page;

        try {

            // to work with cache
            $key = '_endpoint_' . $endpoint . '_page_' . $page . '_limit_' . $limit . '_offset_' . $offset;
            // get from cache
            $response = Cache::get($key);

            if(empty($response)){
                $request = $this->client->request('GET', $url);

                $response = json_decode($request->getBody()->getContents(), true);
                // $response = $request->getBody()->getContents();
                
                // replace results
                if(!is_null($response['previous']) || !empty($response['previous'])){
                    $response['previous'] = $this->replaceVehicle->replacePrevious($response['previous']);
                }

                if(!is_null($response['next']) || !empty($response['next'])){
                    $response['next'] =  $this->replaceVehicle->replaceNext($response['next']);
                }

                if(count($response['results'])) {
                    $response['results'] = $this->replaceVehicle->filterResults($response['results']);
                }                

                // save on cache 24 hours
                Cache::put($key, serialize($response), 86400);

                return $response;

            }else{
                // return from cache 
                return unserialize($response);
            }
        } catch (GuzzleException $e) {
            return false;
        }catch (\Exception $e) {
            return false;
        }
    }

    /**
    * endpoint https://swapi.dev/api/vehicles/{id}
    *
    * @return array|mixed
    */
    public function getVehiclesById($id)
    {
        // endpoint
        $endpoint = 'vehicles/' . $id;

        // url
        $url = $this->api_url . $endpoint;

        try {

            // to work with cache
            $key = '_endpoint_' . $endpoint;
            // get from cache
            $response = Cache::get($key);

            if(empty($response)){
                $request = $this->client->request('GET', $url);

                $response = json_decode($request->getBody()->getContents(), true);
                // $response = $request->getBody()->getContents();

                $response = $this->replaceVehicle->filterResult($response);

                // save on cache 24 hours
                Cache::put($key, serialize($response), 86400);

                return $response;

            }else{
                // return from cache 
                return unserialize($response);
            }
        } catch (GuzzleException $e) {
            return false;
        }catch (\Exception $e) {
            return false;
        }
    }
}