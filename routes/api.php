<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\StarwarsController;
use App\Http\Controllers\Api\AuthenticationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::post('register', [AuthController::class, 'register']);
// Route::post('login', [AuthController::class, 'login']);

Route::controller(AuthenticationController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
    Route::post('logout', 'logout');
    Route::post('refresh', 'refresh');
    Route::get('me', 'me');

});

// comment to use JWT auth
/* Route::middleware('auth:sanctum')->group(function() {
    Route::get('people', [StarwarsController::class, 'getPeopleAll']);
    Route::get('people/{id}', [StarwarsController::class, 'getPeopleById']);
    Route::get('planets', [StarwarsController::class, 'getPlanetsAll']);
    Route::get('planets/{id}', [StarwarsController::class, 'getPlanetsById']);
    Route::get('vehicles', [StarwarsController::class, 'getVehiclesAll']);
    Route::get('vehicles/{id}', [StarwarsController::class, 'getVehiclesById']);

}); */

Route::middleware('auth:api')->group(function() {
    Route::get('people', [StarwarsController::class, 'getPeopleAll']);
    Route::get('people/{id}', [StarwarsController::class, 'getPeopleById']);
    Route::get('planets', [StarwarsController::class, 'getPlanetsAll']);
    Route::get('planets/{id}', [StarwarsController::class, 'getPlanetsById']);
    Route::get('vehicles', [StarwarsController::class, 'getVehiclesAll']);
    Route::get('vehicles/{id}', [StarwarsController::class, 'getVehiclesById']);

});